// import { View, Text, Button } from 'react-native'
// import React, { Component } from 'react';

// class App extends Component {
//   constructor () {
//     super();
//     this.state = {
//       counter: 0
//     };
//   }
//   incrementCount () {
//     this.setState({
//       counter: this.state.counter + 1
      
//     });
//   }
//   decrementCount () {
//     if (this.state.counter > 0 ) {
//       this.setState(prevState => ({ counter: prevState.counter - 1 }));
//     }
//   }
//   render () {
//     return (
//       <View>
//         <Text>Count: {this.state.counter}</Text>
//         <Button onPress={this.decrementCount.bind(this)}title="Minus">-</Button>
//         <Button onPress={this.incrementCount.bind(this)}title="plus">+</Button>
//       </View>
//     );
//   }
// }

// export default App

import React, { Component } from 'react';
import { View, Text, TextInput, Button, Alert, SafeAreaView } from 'react-native';

var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");

class RegisterComponent extends Component {
  state = {
    username: '',
    email: '',
    password: ''
  }

  handleUsernameChange = (username) => {
    this.setState({ username });
  }

  handleEmailChange = (email) => {
    this.setState({ email });
  }

  handlePasswordChange = (password) => {
    this.setState({ password });
  }

  handleRegister = () => {
    const { username, email, password } = this.state;
    const data = {
      username,
      email,
      password
    };

    fetch('https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json', {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(data),
    })
    .then(response => response.json())
    .then(responseData => {
      Alert.alert('Registration Success', 'Your account has been registered successfully');
      console.log(data)
    })
    .catch(error => {
      console.error(error);
      Alert.alert('Registration Failed', 'An error occurred while registering your account');
    });
  }

  render() {
    return (
      <SafeAreaView>
      <View>
        <Text>Register</Text>
        <TextInput
          placeholder="Username"
          onChangeText={this.handleUsernameChange}
          value={this.state.username}
          style = {{ justifyContent : 'center',padding: 20, alignItems :'center'}}
        />
        <TextInput
          placeholder="Email"
          onChangeText={this.handleEmailChange}
          value={this.state.email}
          style = {{padding:20,}}
        />
        <TextInput
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={this.handlePasswordChange}
          value={this.state.password}
          style = {{padding:20,}}
        />
        <Button
          title="Register"
          onPress={this.handleRegister}
        />
      </View>
      </SafeAreaView>
    );
  }
}

export default RegisterComponent;